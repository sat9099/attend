# Attendance System

python env =  3.5
Django = 1.9.1

tested on both linux and mac os box 

# python-pip 
https://pip.pypa.io/en/stable/installing/#do-i-need-to-install-pip

-pip3 install virtualenv

# Checklist 
 
- virtualenv env -p python3
- source env/bin/activate
- git clone https://sat9099@bitbucket.org/sat9099/attend.git
- pip install -r requirements.txt

# Or u can directly run 
 - source attend/env/bin/activate
 - git clone https://sat9099@bitbucket.org/sat9099/attend.git
 - cd /attend/attendance
 - python manage.py runserver
 - Check 127.0.0.1:8000
 - check 127.0.0.1:8000/admin to check admin panel 
 - Credentials : username:admin, password:pass@123



# open postman and send your requests 
 - Note : The repository comes with sample data for 3 employees 

 - eg request looks something like this 
 - postman request 1 : 127.0.01:8000/api/attendance/ (to start and end day ) method: post
 - postman request 2 : 127.0.01:8000/api/break/ (to start and stop break ) method : post
 - postman request 3 : 127.0.01:8000/api/attendance/?start_day=2017-05-1&end_day=2017-05-5&employee_id=1      (to get employee details ) method = get 

 - response : 
 {
    "attendance": [
        {
            "date": "2017-05-01",
            "start_time": "10:05:35",
            "total_minutes_worked": 411,
            "end_time": "18:05:35"
        },
        {
            "date": "2017-05-02",
            "start_time": "10:05:35",
            "total_minutes_worked": 411,
            "end_time": "18:05:35"
        },
        {
            "date": "2017-05-03",
            "start_time": "10:05:35",
            "total_minutes_worked": 411,
            "end_time": "18:05:35"
        },
        {
            "date": "2017-05-04",
            "start_time": "10:05:35",
            "total_minutes_worked": 411,
            "end_time": "18:05:35"
        },
        {
            "date": "2017-05-05",
            "start_time": "10:05:35",
            "total_minutes_worked": 411,
            "end_time": "18:05:35"
        }
    ]
}

Please feel free to contact me if you have any disturbance in running app