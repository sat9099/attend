from django.contrib import admin
from .models import Employee, Event


admin.site.register(Employee) # register Employee information in admin panel 
admin.site.register(Event) # register Event Information in admin panel 

