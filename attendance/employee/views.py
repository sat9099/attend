from rest_framework import viewsets
from rest_framework.views import APIView
from rest_framework.response import Response
from django.http import HttpResponse, JsonResponse
from employee.models import Employee, Event
from datetime import datetime, timedelta, date


class Work(APIView):
	"""
		Function to get / post information of employee timings for particular data and time 
	"""

	def get(self, request, *args, **kwargs):
		error = {}
		try:
			start_date = datetime.strptime(request.GET.get('start_day', None), "%Y-%m-%d")
			end_date = datetime.strptime(request.GET.get('end_day', None), "%Y-%m-%d")
		except:
			error["error"] = "Incorrect Format for start-day or end_day"
			return Response(error)
		try:
			emp_id = request.GET.get('employee_id', None)
			employee = Employee.objects.get(id = emp_id)
		except:
			error["error"] = "Employee Id Doesn't Exist"
			return Response(error)
		attendance = []
		while start_date <= end_date:
			events = Event.objects.filter(employee = employee, date = start_date)
			if len(events) != 2:
				error["error"] = "Data for those days not available - Please  select a data range for which data exists"
				return Response(error)
			total_minutes_worked = 0
			daily = {}
			for i in events:
				if i.event == "work":
					time = datetime.combine(date.today(), i.end_time) - datetime.combine(date.today(), i.start_time)
					hours = time.seconds//3600
					minutes = (time.seconds//60)%60
					total_minutes_worked += minutes + (hours * 60)
					daily = {
						"date":i.date,
						"start_time": i.start_time,	
						"end_time": i.end_time
					}
				elif i.event == "break":
					time = datetime.combine(date.today(), i.end_time) - datetime.combine(date.today(), i.start_time)
					hours = time.seconds//3600
					minutes = (time.seconds//60)%60
					total_minutes_worked -= minutes + (hours * 60)
			daily["total_minutes_worked"] = total_minutes_worked
			attendance.append(daily)
			start_date += timedelta(days = 1)
		response = {"attendance" : attendance}	
		return Response(response) 



	def post(self, request):

		
		error = {}
		emp_id = request.data["employee_id"]
		date = request.data["date"]
		time = request.data["time"]
		event = request.data["event"]
		try:
			employee = Employee.objects.get(id = emp_id)
		except:
			error["error"] = "Employee with that id doesn't exist"
			return Response(error)
		try:
			date = datetime.strptime(date, "%Y-%m-%d")
			time = datetime.strptime(time, "%H:%M:%S")
		except:
			error["error"] = "Date and Time Should be in ISO Format"
			return Response(error)
		if event == "start":
			if  len(Event.objects.filter(employee = employee, date = date.date(), event = "work")) != 0:
				error["error"] = "Employee already has a start acitivity for this event"
				return Response(error)
			event1 = Event(employee = employee, date = date.date(), start_time = time.time(), event = "work")
			event1.save()
			response = {}
			attendance = {
				"date" : event1.date,
				"start_time" : event1.start_time,
				"end_time" : None,
				"total_minutes_worked" : 0 
			}
			response["attendance"] = attendance		
		elif event == "end":
			try:
				event1 = Event.objects.get(employee = employee, date = date.date(), event = "work")
			except:
				error["error"] = "Can't Have end activity without start activity"
				return Response(error)
			event1.end_time = time.time()
			event1.save()
			response = {}
			attendance = {
				"date" : event1.date,
				"start_time" : event1.start_time,
				"end_time" : event1.end_time,
				"total_minutes_worked" : 0 
			}
			response["attendance"] = attendance
		try:
			return Response(response)
		except:
			error["error"] = "Event must be start or end"
			return Response(error)

class Break(APIView):
	"""
		Funciton to post break information Start/ Stop break event 
	"""
	def post(self, request):
		error = {}
		emp_id = request.data["employee_id"]
		date = request.data["date"]
		time = request.data["time"]
		event = request.data["event"]
		try:
			employee = Employee.objects.get(id = emp_id)
		except:
			error["error"] = "Employee with that id doesn't exist"
			return Response(error)
		try:
			date = datetime.strptime(date, "%Y-%m-%d")
			time = datetime.strptime(time, "%H:%M:%S")
		except:
			error["error"] = "Date and Time Should be in ISO Format"
			return Response(error)
		if event == "start":
			if  len(Event.objects.filter(employee = employee, date = date.date(), event = "break")) != 0:
				error["error"] = "Employee already has a start acitivity for this event"
				return Response(error)
			event1 = Event(employee = employee, date = date.date(), start_time = time.time(), event = "break")
			event1.save()
			response = {}
			breaks = {
				"date" : event1.date,
				"start_time" : event1.start_time,
				"end_time" : None,
				"total_minutes_worked" : 0 
			}
			response["break"] = breaks		
		elif event == "stop":
			try:
				event1 = Event.objects.get(employee = employee, date = date.date(), event = "break")
			except:
				error["error"] = "Can't Have stop activity without start activity"
				return Response(error)
			event1.end_time = time.time()
			event1.save()
			response = {}
			breaks = {
				"date" : event1.date,
				"start_time" : event1.start_time,
				"end_time" : event1.end_time,
				"total_minutes_worked" : 0 
			}
			response["break"] = breaks
		try:
			return Response(response)
		except:
			error["error"] = "Event must be start or stop"
			return Response(error)

