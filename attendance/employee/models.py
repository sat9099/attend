from django.db import models
from django_extensions.db.models import TimeStampedModel
# Create your models here.

"""
	Employee Table to store employee information
"""
class Employee(TimeStampedModel):
	id = models.AutoField(primary_key = True)
	name = models.CharField(max_length = 100)
	
	def __str__(self):
		return str(self.id) + " - " + self.name


"""
	To Store event Information like Start / End day and Start / Stop Break 
"""
class Event(TimeStampedModel):
	employee = models.ForeignKey(Employee)
	date = models.DateField() 
	start_time = models.TimeField()
	end_time = models.TimeField(null = True, blank = True)
	event = models.CharField(max_length = 100)

	def __str__(self):
		return str(self.employee.id) + " - " + self.event + "(" + str(self.date) + ")"